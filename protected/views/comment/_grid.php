<?php

/** @var Comment $model */


/*
 * This CGV is rendered by both PostController ('post/admin') and by
 * CommentController when 'comment/adminGrid' runs to render an ajax update of this
 * partial. CButtonColumn by default calls the current controller to create URLs
 * for the buttons but that won't work when this crid is rendered by the
 * PostController. That's why we override the URL and use the application's createUrl
 * instead.
 */

function myButton($action)
{
	return array(
		'url'=>'Yii::app()->createUrl("comment/'.$action.'",array("id"=>$data->primaryKey))',
	);
}

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comment-grid',
	'dataProvider'=>$model->search(),
	'ajaxUrl'=>array('comment/adminGrid'),
	'filter'=>$model,
	'columns'=>array(
		'content',
		'status',
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		'author',
		'email',
		'post_id',
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array(
				'view'=>myButton('view'),
				'update'=>myButton('update'),
				'delete'=>myButton('delete'),
			),
		),
	),
));