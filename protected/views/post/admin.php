<?php

/** @var PostController $this */
/** @var Post $model */
/** @var Comment $comment */
/** @var array breadcrumbs */
$this->breadcrumbs=array(
	'Manage Posts',
);
?>
<h1>Manage Posts</h1>

<?php
$this->renderPartial('_grid', array('model' => $model));
$this->renderPartial('/comment/_grid', array('model' => $comment));
