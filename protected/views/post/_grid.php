<?php

/** @var Post $model */

$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'post-grid',
	'dataProvider'=>$model->search(),
    'ajaxUrl'=>array('post/adminGrid'),
	'filter'=>$model,
	'columns'=>array(
        'id',
		array(
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->title), $data->url)'
		),
		array(
			'name'=>'status',
			'value'=>'Lookup::item("PostStatus",$data->status)',
			'filter'=>Lookup::items('PostStatus'),
		),
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
));