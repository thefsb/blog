#Multiple CGridView on one page with ajax updating

Discussions about how to handle multiple CGridView one page in a webapp based on Yii Framework 1.1 typically confuse me.
I wanted to have something to point at when I engage in these conversations. This repo has something to point at.

##About the code

This is based on the Yii Blog tutorial lifted from Yii 1.1.14.

I made these changes:

- Moved the CGV from `post/admin` into a `post/_grid` partial
- Added an analogous `comment/_grid` partial with an admin GCV for comments and a search method and rules to the `Comment` model
- Added `adminGrid` actions to both post and comment controllers that render their corresponding _grid partials for CGV AJAX update
- Extracted a `gridModel()` static method in both controllers that the AJAX update and `post/admin` actions can use
- Changed `post/admin` view to render both the `_grid` partials passing in their respective search models
- Reconfigured the app with Gii, a MySQL db, without URL manager
- Adjusted the entry script for my Yii framework location and to turn debugging back on
- Added a standard Gii CRUD `actionView` to `CommentController` and corresponding view (just so its grid can have all three buttons)

To study these changes, compare the two git tags: **yii-1.1.14-version** and **Multi-CGV**.